CREATE SEQUENCE hibernate_sequence START WITH 9;

CREATE TABLE customer (
    id BIGINT NOT NULL PRIMARY KEY,
    lat DOUBLE NOT NULL,
    lon DOUBLE NOT NULL
);

CREATE TABLE restaurant (
    id BIGINT NOT NULL PRIMARY KEY,
    lat DOUBLE NOT NULL,
    lon DOUBLE NOT NULL
);

CREATE TABLE purchase_order (
    id BIGINT NOT NULL PRIMARY KEY,
    restaurant_id BIGINT NOT NULL,
    customer_id BIGINT NOT NULL,
    pickup TIMESTAMP,
    delivery TIMESTAMP,
    CONSTRAINT fk_purchase_order_01 FOREIGN KEY(restaurant_id) REFERENCES restaurant(id),
    CONSTRAINT fk_purchase_order_02 FOREIGN KEY(customer_id) REFERENCES customer(id)
);

CREATE TABLE route (
    id BIGINT NOT NULL PRIMARY KEY,
    restaurant_id BIGINT NOT NULL,
    CONSTRAINT fk_route_01 FOREIGN KEY(restaurant_id) REFERENCES restaurant(id)
);

CREATE TABLE route_purchase_order (
    route_id BIGINT NOT NULL,
    purchase_order_id BIGINT NOT NULL,
    CONSTRAINT fk_route_purchase_order_01 FOREIGN KEY(route_id) REFERENCES route(id),
    CONSTRAINT fk_route_purchase_order_02 FOREIGN KEY(purchase_order_id) REFERENCES purchase_order(id)
);
