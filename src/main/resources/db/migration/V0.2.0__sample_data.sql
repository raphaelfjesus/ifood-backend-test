-- Restaurants
INSERT INTO restaurant (id, lat, lon) VALUES (1, 0.0, 0.0);
INSERT INTO restaurant (id, lat, lon) VALUES (2, 0.0, 1.0);

-- Customers
INSERT INTO customer (id, lat, lon) VALUES (1, 1.0, 0.0);
INSERT INTO customer (id, lat, lon) VALUES (2, 1.0, 1.0);
INSERT INTO customer (id, lat, lon) VALUES (3, 2.0, 0.0);
INSERT INTO customer (id, lat, lon) VALUES (4, 2.0, 1.0);
INSERT INTO customer (id, lat, lon) VALUES (5, 2.0, 2.0);
INSERT INTO customer (id, lat, lon) VALUES (6, 3.0, 0.0);
INSERT INTO customer (id, lat, lon) VALUES (7, 3.0, 1.0);
INSERT INTO customer (id, lat, lon) VALUES (8, 3.0, 2.0);
INSERT INTO customer (id, lat, lon) VALUES (9, 3.0, 3.0);
INSERT INTO customer (id, lat, lon) VALUES (10, 1.0, 2.0);

-- Orders
INSERT INTO purchase_order (id, restaurant_id, customer_id, pickup, delivery) VALUES (1, 1, 1, '2018-12-26 00:00:00', '2018-12-27 00:00:00');
INSERT INTO purchase_order (id, restaurant_id, customer_id, pickup, delivery) VALUES (2, 1, 2, '2018-12-26 00:00:00', '2018-12-27 00:00:00');
INSERT INTO purchase_order (id, restaurant_id, customer_id, pickup, delivery) VALUES (3, 1, 3, '2018-12-26 00:00:00', '2018-12-27 00:00:00');
INSERT INTO purchase_order (id, restaurant_id, customer_id, pickup, delivery) VALUES (4, 2, 5, '2018-12-26 00:00:00', '2018-12-27 00:00:00');
INSERT INTO purchase_order (id, restaurant_id, customer_id, pickup, delivery) VALUES (5, 2, 10, '2018-12-26 00:00:00', '2018-12-27 00:00:00');

-- Routes
--INSERT INTO route (id, restaurant_id) VALUES (1, 1);

-- Route x Orders
--INSERT INTO route_purchase_order (route_id, purchase_order_id) VALUES (1, 1);
