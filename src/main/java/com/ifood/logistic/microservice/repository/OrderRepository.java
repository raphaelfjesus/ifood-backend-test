package com.ifood.logistic.microservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ifood.logistic.microservice.domain.entity.Order;
import com.ifood.logistic.microservice.repository.custom.OrderRepositoryCustom;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>, OrderRepositoryCustom {

	@Query(value = "SELECT * FROM PURCHASE_ORDER o WHERE NOT EXISTS(SELECT ro.* FROM ROUTE_PURCHASE_ORDER ro WHERE o.id = ro.purchase_order_id) ORDER BY o.pickup, o.delivery, o.id", nativeQuery = true)
	List<Order> getUndelivered();

}
