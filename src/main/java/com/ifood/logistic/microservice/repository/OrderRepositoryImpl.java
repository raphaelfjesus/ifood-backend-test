package com.ifood.logistic.microservice.repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.ifood.logistic.microservice.domain.entity.Order;
import com.ifood.logistic.microservice.domain.entity.Order_;
import com.ifood.logistic.microservice.domain.entity.Restaurant;
import com.ifood.logistic.microservice.domain.entity.Restaurant_;
import com.ifood.logistic.microservice.repository.custom.OrderRepositoryCustom;

@Repository
public class OrderRepositoryImpl implements OrderRepositoryCustom {

	private EntityManager em;

	public OrderRepositoryImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public Page<Order> search(final Long restaurantId, final LocalDateTime deliveryStart, final LocalDateTime deliveryEnd, final Pageable pageable) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Order> cq = cb.createQuery(Order.class);

		Root<Order> orderFrom = cq.from(Order.class);
		orderFrom.fetch(Order_.restaurant);
		orderFrom.fetch(Order_.customer);

		Join<Order, Restaurant> restaurantJoin = orderFrom.join(Order_.restaurant);

		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(restaurantJoin.get(Restaurant_.id), restaurantId));

		if (deliveryStart != null) {
			predicates.add(cb.greaterThanOrEqualTo(orderFrom.get(Order_.delivery), deliveryStart));
		}

		if (deliveryEnd != null) {
			predicates.add(cb.lessThanOrEqualTo(orderFrom.get(Order_.delivery), deliveryEnd));
		}

		CriteriaQuery<Order> select = cq.where(predicates.toArray(new Predicate[predicates.size()]));

		Long count = countResult(restaurantId, deliveryStart, deliveryEnd);
		if (count == 0) {
			return new PageImpl<>(Collections.emptyList(), pageable, count);
		}

		TypedQuery<Order> typedQuery = em.createQuery(select);
		typedQuery.setFirstResult((int) pageable.getOffset());
		typedQuery.setMaxResults(pageable.getPageSize());

		return new PageImpl<>(typedQuery.getResultList(), pageable, count);
	}

	private Long countResult(final Long restaurantId, final LocalDateTime deliveryStart, final LocalDateTime deliveryEnd) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);

		Root<Order> orderFrom = cq.from(Order.class);
		Join<Order, Restaurant> restaurantJoin = orderFrom.join(Order_.restaurant);

		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(restaurantJoin.get(Restaurant_.id), restaurantId));

		if (deliveryStart != null) {
			predicates.add(cb.greaterThanOrEqualTo(orderFrom.get(Order_.delivery), deliveryStart));
		}

		if (deliveryEnd != null) {
			predicates.add(cb.lessThanOrEqualTo(orderFrom.get(Order_.delivery), deliveryEnd));
		}

		CriteriaQuery<Long> countSelect = cq.select(cb.count(orderFrom.get(Order_.id)));
		countSelect.where(predicates.toArray(new Predicate[predicates.size()]));

		return em.createQuery(countSelect).getSingleResult();
	}

}
