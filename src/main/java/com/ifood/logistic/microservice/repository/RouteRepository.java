package com.ifood.logistic.microservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ifood.logistic.microservice.domain.entity.Route;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {

}
