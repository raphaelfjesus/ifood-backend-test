package com.ifood.logistic.microservice.repository.custom;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ifood.logistic.microservice.domain.entity.Order;

public interface OrderRepositoryCustom {

	Page<Order> search(final Long restaurantId, final LocalDateTime deliveryStart, final LocalDateTime deliveryEnd, final Pageable pageable);

}
