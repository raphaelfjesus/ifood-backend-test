package com.ifood.logistic.microservice.exception;

public class CustomerNotFoundException extends RecordNotFoundException {

	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException() {
		super("Customer not found");
	}

}
