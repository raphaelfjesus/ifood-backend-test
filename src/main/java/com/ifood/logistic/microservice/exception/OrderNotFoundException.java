package com.ifood.logistic.microservice.exception;

public class OrderNotFoundException extends RecordNotFoundException {

	private static final long serialVersionUID = 1L;

	public OrderNotFoundException() {
		super("Order not found");
	}

}
