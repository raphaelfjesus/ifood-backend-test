package com.ifood.logistic.microservice.exception;

public class RestaurantNotFoundException extends RecordNotFoundException {

	private static final long serialVersionUID = 1L;

	public RestaurantNotFoundException() {
		super("Restaurant not found");
	}

}
