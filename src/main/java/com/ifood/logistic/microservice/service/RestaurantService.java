package com.ifood.logistic.microservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ifood.logistic.microservice.domain.entity.Restaurant;
import com.ifood.logistic.microservice.exception.RestaurantNotFoundException;
import com.ifood.logistic.microservice.repository.RestaurantRepository;

@Service
public class RestaurantService {

	@Autowired
	private RestaurantRepository restaurantRepository;

	public Restaurant create(final Restaurant restaurant) {
		return restaurantRepository.save(restaurant);
	}

	public Restaurant update(final Restaurant restaurant) {
		if (!restaurantRepository.existsById(restaurant.getId())) {
			throw new RestaurantNotFoundException();
		}

		return restaurantRepository.save(restaurant);
	}

	public Page<Restaurant> search(final Pageable pageable) {
		return restaurantRepository.findAll(pageable);
	}

	public Restaurant get(final Long id) {
		return restaurantRepository.findById(id).orElseThrow(RestaurantNotFoundException::new);
	}

}
