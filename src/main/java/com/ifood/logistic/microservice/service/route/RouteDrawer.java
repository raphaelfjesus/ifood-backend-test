package com.ifood.logistic.microservice.service.route;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import lombok.AllArgsConstructor;
import lombok.Getter;

import org.springframework.stereotype.Component;

import com.ifood.logistic.microservice.domain.Location;
import com.ifood.logistic.microservice.domain.OrderedRoute;
import com.ifood.logistic.microservice.domain.SubRoute;

@Component
public class RouteDrawer {

	private static int width = 600;
	private static int height = 600;
	private static int margin = 200;
	private static int marginNode = 1;
	private static int mX = width - 2 * margin;
	private static int mY = height - 2 * margin;

	public byte[] drawRoutes(final List<SubRoute> routes) throws IOException {
		BufferedImage output = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		Graphics2D graphics = output.createGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, width, height);
		graphics.setColor(Color.BLACK);

		Map<SubRoute, List<Location>> map = joinLocations(routes);

		// Identify boundary coordinates
		List<Location> locations = map.values().stream().flatMap(Collection::stream).collect(Collectors.toList());

		double minX = locations.stream().map(Location::getLat).reduce(Double.MAX_VALUE, Math::min);
		double maxX = locations.stream().map(Location::getLat).reduce(Double.MIN_VALUE, Math::max);
		double minY = locations.stream().map(Location::getLon).reduce(Double.MAX_VALUE, Math::min);
		double maxY = locations.stream().map(Location::getLon).reduce(Double.MIN_VALUE, Math::max);

		// Calculate dimensions of the x and y axes
		Dimension dimension = calculateDimension(minX, maxX, minY, maxY);

		// Draw lines
		Coordinate coordinate = null;

		for (Map.Entry<SubRoute, List<Location>> entry : map.entrySet()) {
			for (int j = 1; j < entry.getValue().size(); j++) {
				coordinate = calculateLine(entry.getValue().get(j - 1), entry.getValue().get(j), dimension, minX, maxX, minY, maxY);

				graphics.drawLine(coordinate.getX1(), coordinate.getY1(), coordinate.getX2(), coordinate.getY2());
			}
		}

		// Draw points
		int fillX = 0;
		int fillY = 0;
		String idTemplate = "(%s, %s)";

		for (Location location : locations) {
			fillX = (int) ((dimension.getXAxis()) * ((location.getLat() - minX) / (maxX - minX) - 0.5) + (double) mX / 2) + margin;
			fillY = (int) ((dimension.getYAxis()) * (0.5 - (location.getLon() - minY) / (maxY - minY)) + (double) mY / 2) + margin;

			graphics.setColor(location.isDepot() ? Color.RED : Color.BLACK);
			graphics.fillRect(fillX - 3 * marginNode, fillY - 3 * marginNode, 6 * marginNode, 6 * marginNode); //2244

			graphics.setColor(Color.GRAY);
			graphics.drawString(String.format(idTemplate, location.getLat(), location.getLon()), fillX + 6 * marginNode, fillY + 6 * marginNode); //88
		}

		String template = "VRP Solution for %d orders in %d restaurants.";
		long orderAmount = routes.stream().map(r -> r.getOrderedRoutes().size()).reduce((r1, r2) -> r1 + r2).orElse(0);

		graphics.setColor(Color.BLACK);
		graphics.drawString(String.format(template, orderAmount, routes.size()), 10, 30);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ImageIO.write(output, "PNG", outputStream);

		return outputStream.toByteArray();
	}

	private Coordinate calculateLine(final Location locationA, final Location locationB, final Dimension dimension, double minX, double maxX, double minY, double maxY) {
		int x1 = (int) ((dimension.getXAxis()) * ((locationA.getLat() - minX) / (maxX - minX) - 0.5) + (double) mX / 2) + margin;
		int y1 = (int) ((dimension.getYAxis()) * (0.5 - (locationA.getLon() - minY) / (maxY - minY)) + (double) mY / 2) + margin;

		int x2 = (int) ((dimension.getXAxis()) * ((locationB.getLat() - minX) / (maxX - minX) - 0.5) + (double) mX / 2) + margin;
		int y2 = (int) ((dimension.getYAxis()) * (0.5 - (locationB.getLon() - minY) / (maxY - minY)) + (double) mY / 2) + margin;

		return new Coordinate(x1, y1, x2, y2);
	}

	private Dimension calculateDimension(double minX, double maxX, double minY, double maxY) {
		int xAxis;
		int yAxis;

		if ((maxX - minX) > (maxY - minY)) {
			xAxis = mX;
			yAxis = (int) ((xAxis) * (maxY - minY) / (maxX - minX));

			if (yAxis > mY) {
				yAxis = mY;
				xAxis = (int) ((yAxis) * (maxX - minX) / (maxY - minY));
			}

		} else {
			yAxis = mY;
			xAxis = (int) ((yAxis) * (maxX - minX) / (maxY - minY));

			if (xAxis > mX) {
				xAxis = mX;
				yAxis = (int) ((xAxis) * (maxY - minY) / (maxX - minX));
			}
		}

		return new Dimension(xAxis, yAxis);
	}

	private Map<SubRoute, List<Location>> joinLocations(final List<SubRoute> routes) {
		Map<SubRoute, List<Location>> map = new HashMap<>();

		routes.stream().forEach(route -> {
			List<Location> locations = new LinkedList<>();
			locations.add(route.getDepot());
			locations.addAll(route.getOrderedRoutes().stream().map(OrderedRoute::getDestination).collect(Collectors.toList()));
			locations.add(route.getDepot());

			map.put(route, locations);
		});

		return map;
	}

	@Getter
	@AllArgsConstructor
	private class Coordinate {

		private int x1;
		private int y1;
		private int x2;
		private int y2;

	}

	@Getter
	@AllArgsConstructor
	private class Dimension {

		private double xAxis;
		private double yAxis;

	}

}
