package com.ifood.logistic.microservice.service.route;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ifood.logistic.microservice.domain.CalculatedRoute;

@Component
class RouteComparator implements Comparator<CalculatedRoute> {

	private static List<Comparator<CalculatedRoute>> comparators = new ArrayList<>();
	static {
		comparators.add(comparingAmountRoutes());
		comparators.add(comparingNewRoutePickupTime());
		comparators.add(comparingNewRouteDeliveryTime());
		comparators.add(comparingMaxLateDeliveryTime());
		comparators.add(comparingMinTravelTime());
	}

	@Override
	public final int compare(final CalculatedRoute route1, final CalculatedRoute route2) {
		int result = 0;

		for (Comparator<CalculatedRoute> comparator : comparators) {
			result = comparator.compare(route1, route2);

			if (result != 0) {
				return result;
			}
		}

		return result;
	}

	private static Comparator<CalculatedRoute> comparingAmountRoutes() {
		return Comparator.comparingInt(CalculatedRoute::getNumberOfOrderedRoutes).reversed();
	}

	private static Comparator<CalculatedRoute> comparingNewRoutePickupTime() {
		return (r1, r2) -> isDepot(r1, r2) || isOldRoute(r1, r2) ? 0 : r1.getPickup().compareTo(r2.getPickup());
	}

	private static Comparator<CalculatedRoute> comparingNewRouteDeliveryTime() {
		return (r1, r2) -> isDepot(r1, r2) || isOldRoute(r1, r2) ? 0 : r1.getLastDelivered().getDelivery().compareTo(r2.getLastDelivered().getDelivery());
	}

	private static Comparator<CalculatedRoute> comparingMaxLateDeliveryTime() {
		return Comparator.comparingDouble(CalculatedRoute::getLateDeliveryTime).reversed();
	}

	private static Comparator<CalculatedRoute> comparingMinTravelTime() {
		return Comparator.comparingDouble(CalculatedRoute::getTravelTime);
	}

	private static boolean isDepot(final CalculatedRoute route1, final CalculatedRoute route2) {
		return route1.getDestination().isDepot() || route2.getDestination().isDepot();
	}

	private static boolean isOldRoute(final CalculatedRoute route1, final CalculatedRoute route2) {
		return route1.getNumberOfOrderedRoutes() != 1 || route2.getNumberOfOrderedRoutes() != 1;
	}

}
