package com.ifood.logistic.microservice.service.route;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.function.BiFunction;

import org.springframework.stereotype.Component;

import com.ifood.logistic.microservice.domain.CalculatedRoute;
import com.ifood.logistic.microservice.domain.CalculatedRouteImpl;
import com.ifood.logistic.microservice.domain.SubRoute;
import com.ifood.logistic.microservice.domain.entity.Restaurant;
import com.ifood.logistic.microservice.domain.entity.Route;

@Component
class RouteSimulator {

	public CalculatedRoute simulate(final CalculatedRoute routeA, final CalculatedRoute routeB) {
		BiFunction<LocalDateTime, Double, LocalDateTime> sum = (deliveryTime, elapsedTime) -> deliveryTime.plus(elapsedTime.longValue(), ChronoField.MILLI_OF_DAY.getBaseUnit());

		CalculatedRouteImpl calculatedRoute = new CalculatedRouteImpl();
		calculatedRoute.setOrigin(routeA.getOrigin());
		calculatedRoute.setDestination(routeB.getDestination());
		calculatedRoute.setLocations(routeA.getLocations());
		calculatedRoute.getLocations().add(routeB.getDestination());

		calculatedRoute.setTravelTime(routeA.getTravelTime() + routeB.getTravelTime());
		calculatedRoute.setPickup(routeA.getPickup());
		calculatedRoute.setDelivery(sum.apply(routeA.getDelivery(), routeB.getTravelTime()));

		calculatedRoute.setCurrentDepot(routeB.getCurrentDepot() != null ? routeB.getCurrentDepot() : routeA.getCurrentDepot());
		calculatedRoute.setCurrentDepotArrivalTime(calculatedRoute.getDestination().isDepot() ? calculatedRoute.getDelivery() : routeA.getCurrentDepotArrivalTime());

		calculatedRoute.getOrderedRoutes().addAll(routeA.getOrderedRoutes());
		calculatedRoute.getOrderedRoutes().addAll(routeB.getOrderedRoutes());
		calculatedRoute.setLastDelivered(routeB.getLastDelivered() != null ? routeB.getLastDelivered() : routeA.getLastDelivered());
		calculatedRoute.setLateDeliveryTime(routeA.getLateDeliveryTime());

		simulateSubRoutes(calculatedRoute, routeA, routeB);

		return calculatedRoute;
	}

	private void simulateSubRoutes(CalculatedRoute calculatedRoute, CalculatedRoute routeA, CalculatedRoute routeB) {
		calculatedRoute.getSubRoutes().addAll(routeA.getSubRoutes());

		calculatedRoute.getSubRoutes().remove(routeA.getPendingSubRoute());

		SubRoute pending = new Route((Restaurant) routeA.getCurrentDepot());
		routeA.getPendingSubRoute().getOrderedRoutes().forEach(pending::addOrderedRoute);
		calculatedRoute.getSubRoutes().add(pending);

		if (routeB.getDestination().isDepot()) {
			pending = new Route((Restaurant) routeB.getDestination());
			calculatedRoute.getSubRoutes().add(pending);

		} else {
			pending.addOrderedRoute(routeB.getLastDelivered());
		}

		calculatedRoute.setPendingSubRoute(pending);
	}

}
