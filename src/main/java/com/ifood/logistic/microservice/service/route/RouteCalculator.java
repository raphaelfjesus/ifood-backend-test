package com.ifood.logistic.microservice.service.route;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ifood.logistic.microservice.configuration.AppConfiguration;
import com.ifood.logistic.microservice.domain.AlternativeRoute;
import com.ifood.logistic.microservice.domain.AlternativeRouteImpl;
import com.ifood.logistic.microservice.domain.CalculatedRoute;
import com.ifood.logistic.microservice.domain.CalculatedRouteImpl;
import com.ifood.logistic.microservice.domain.CalculatedRouteMap;
import com.ifood.logistic.microservice.domain.Location;
import com.ifood.logistic.microservice.domain.OrderedRoute;
import com.ifood.logistic.microservice.domain.Route;
import com.ifood.logistic.microservice.domain.entity.Restaurant;

@Component
public class RouteCalculator {

	@Autowired
	private RouteComparator routeComparator;

	@Autowired
	private RouteConstraint routeConstraint;

	@Autowired
	private RouteSimulator routeSimulator;

	private AppConfiguration config;

	public RouteCalculator(AppConfiguration config) {
		this.config = config;
	}

	public CalculatedRoute calculate(final List<Route> routes) {
		CalculatedRouteMap routeMap = new CalculatedRouteMap();

		for (Route routeA : routes) {
			for (Route routeB : routes) {

				// Origin to Destination
				if (routeA.equals(routeB)) {
					routeMap.putIfAbsent(calculateRoute(routeA));
					continue;
				}

				// Destination to Destination
				if (routeA.getOrigin().equals(routeB.getOrigin())) {
					routeMap.putIfAbsent(calculateRoute(new AlternativeRouteImpl(routeA.getDestination(), (OrderedRoute) routeB)));
				}

				// Destination to Origin
				routeMap.putIfAbsent(calculateRoute(new AlternativeRouteImpl(routeA.getDestination(), routeB.getOrigin())));
			}
		}

		CalculatedRoute rootRoute = routeMap.getRoutes().stream().filter(r -> r.getOrigin().isDepot()).sorted(routeComparator).findFirst().orElse(null);

		List<CalculatedRoute> pendingRoutes = new ArrayList<>();
		List<CalculatedRoute> completedRoutes = new ArrayList<>();

		// @formatter:off
		Consumer<CalculatedRoute> consumer = root -> {
			routeMap.get(root.getDestination()).stream()
					.filter(candidate -> routeConstraint.check(candidate, root, routeMap))
					.sorted(routeComparator)
					.forEach(newSubRoute -> {
						CalculatedRoute calculatedSubRoute = routeSimulator.simulate(root, newSubRoute);
						calculatedSubRoute.setLateDeliveryTime(calculateLateDeliveryTime(calculatedSubRoute));

						pendingRoutes.add(calculatedSubRoute);
					});

			pendingRoutes.remove(root);
			completedRoutes.add(root);
		};
		// @formatter:on

		while (rootRoute != null) {
			consumer.accept(rootRoute);

			if (pendingRoutes.isEmpty() || rootRoute.getNumberOfOrderedRoutes() == routeMap.getOrderedRoutes().size()) {
				break;
			}

			rootRoute = pendingRoutes.stream().sorted(routeComparator).findFirst().orElse(null);
		}

		return completedRoutes.stream().sorted(routeComparator).findFirst().orElse(null);
	}

	private CalculatedRoute calculateRoute(final Route route) {
		Double travelTime = calculateTravelTime(route.getOrigin(), route.getDestination());

		CalculatedRouteImpl calculatedRoute = new CalculatedRouteImpl();
		calculatedRoute.setDestination(route.getDestination());
		calculatedRoute.setOrigin(route.getOrigin());
		calculatedRoute.setPickup(route.getPickup());
		calculatedRoute.setDelivery(calculateDelivery(route, travelTime));
		calculatedRoute.setCurrentDepotArrivalTime(calculateDepotArrival(route));
		calculatedRoute.setTravelTime(travelTime);
		calculatedRoute.setOrderedRoutes(route.isAlternative() ? ((AlternativeRoute) route).getOrderedRoutes() : Collections.singletonList((OrderedRoute) route));

		if (route.getDestination().isDepot()) {
			calculatedRoute.setCurrentDepot(route.getDestination());

		} else if (route.getOrigin().isDepot()) {
			calculatedRoute.setCurrentDepot(route.getOrigin());
		}

		calculatedRoute.addSubRoute(new com.ifood.logistic.microservice.domain.entity.Route((Restaurant) calculatedRoute.getCurrentDepot()));

		return calculatedRoute;
	}

	private LocalDateTime calculateDepotArrival(final Route route) {
		return route.getDestination().isDepot() ? route.getDelivery() : route.getPickup();
	}

	private LocalDateTime calculateDelivery(final Route route, final Double travelTime) {
		return route.getDestination().isDepot() ? route.getDelivery() : route.getPickup().plus(travelTime.longValue(), ChronoField.MILLI_OF_DAY.getBaseUnit());
	}

	private Double calculateLateDeliveryTime(final CalculatedRoute route) {
		if (route.getDestination().isDepot()) {
			return 0.0;
		}

		Double lateTime = Double.valueOf(ChronoUnit.NANOS.between(route.getDelivery(), route.getLastDelivered().getDelivery()));
		if (lateTime.compareTo(Double.valueOf(0)) >= 0) {
			return 0.0;
		}

		return route.getLateDeliveryTime() + lateTime;
	}

	private Double calculateTravelTime(final Location origin, final Location destination) {
		return calculateTravelDistance(origin, destination) / config.getRoute().getDistanceUnit() * config.getRoute().getDistanceTimeUnit();
	}

	private Double calculateTravelDistance(final Location origin, final Location destination) {
		return calculateEuclideanDistance(origin, destination);
	}

	private Double calculateEuclideanDistance(final Location origin, final Location destination) {
		Double powLat = Math.pow(destination.getLat() - origin.getLat(), 2);
		Double powLon = Math.pow(destination.getLon() - origin.getLon(), 2);

		return Math.sqrt(powLat + powLon);
	}

}
