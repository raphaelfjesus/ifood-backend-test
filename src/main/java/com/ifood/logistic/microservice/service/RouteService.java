package com.ifood.logistic.microservice.service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifood.logistic.microservice.domain.CalculatedRoute;
import com.ifood.logistic.microservice.domain.entity.Route;
import com.ifood.logistic.microservice.repository.RouteRepository;
import com.ifood.logistic.microservice.service.route.RouteCalculator;
import com.ifood.logistic.microservice.service.route.RouteDrawer;

@Service
public class RouteService {

	@Autowired
	private OrderService orderService;

	@Autowired
	private RouteRepository routeRepository;

	@Autowired
	private RouteCalculator routeCalculator;

	@Autowired
	private RouteDrawer routeDrawer;

	public List<Route> getAll() {
		CalculatedRoute root = routeCalculator.calculate(orderService.getUndelivered().stream().collect(Collectors.toList()));
		if (root == null) {
			return Collections.emptyList();
		}

		return root.getSubRoutes().stream().map(r -> routeRepository.save((Route) r)).collect(Collectors.toList());
	}

	@SuppressWarnings("squid:S1168")
	public byte[] draw() throws IOException {
		CalculatedRoute root = routeCalculator.calculate(orderService.getUndelivered().stream().collect(Collectors.toList()));
		if (root == null) {
			return null;
		}

		return routeDrawer.drawRoutes(root.getSubRoutes());
	}

}
