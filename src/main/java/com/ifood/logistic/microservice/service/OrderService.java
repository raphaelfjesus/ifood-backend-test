package com.ifood.logistic.microservice.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ifood.logistic.microservice.domain.entity.Order;
import com.ifood.logistic.microservice.exception.OrderNotFoundException;
import com.ifood.logistic.microservice.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	public Order create(final Order order) {
		return orderRepository.save(order);
	}

	public Page<Order> search(final Long restaurantId, final LocalDateTime deliveryStart, final LocalDateTime deliveryEnd, final Pageable pageable) {
		return orderRepository.search(restaurantId, deliveryStart, deliveryEnd, pageable);
	}

	public Order get(final Long id) {
		return orderRepository.findById(id).orElseThrow(OrderNotFoundException::new);
	}

	@Transactional(readOnly = true)
	public List<Order> getUndelivered() {
		return orderRepository.getUndelivered();
	}

}
