package com.ifood.logistic.microservice.service.route;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.function.BiFunction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ifood.logistic.microservice.configuration.AppConfiguration;
import com.ifood.logistic.microservice.domain.CalculatedRoute;
import com.ifood.logistic.microservice.domain.CalculatedRouteMap;
import com.ifood.logistic.microservice.domain.Location;

@Component
class RouteConstraint {

	@Autowired
	private RouteSimulator routeSimulator;

	private AppConfiguration config;

	public RouteConstraint(AppConfiguration config) {
		this.config = config;
	}

	public boolean check(final CalculatedRoute candidate, final CalculatedRoute root, final CalculatedRouteMap routeMap) {
		return checkCapacity(candidate, root) //
				&& checkLateDeliveryLimit(candidate, root) //
				&& checkExistence(candidate, root, routeMap) //
				&& checkPickTime(candidate, root) //
				&& checkUniqueVisit(candidate, root) //
				&& checkDepotWithoutTarget(candidate, root, routeMap);
	}

	private boolean checkCapacity(final CalculatedRoute candidate, final CalculatedRoute root) {
		// Locations:
		// 1. Ori(a) -> Dest(b) = Ori(a,b)
		// 2. Ori(a,b) -> Dest(c) = Ori(a,b,c)
		// 3. Ori(a,b,c) -> Dest(d) = Ori(a,b,c,d)
		// 4. ...
		if (candidate.getDestination().isDepot() || root.getNumberOfLocations() < config.getRoute().getVehicleCapacity()) {
			return true;
		}

		return root.getLocations().stream() //
				.skip((long) root.getNumberOfLocations() - config.getRoute().getVehicleCapacity()) //
				.filter(Location::isNotDepot) //
				.count() < config.getRoute().getVehicleCapacity();
	}

	private boolean checkLateDeliveryLimit(final CalculatedRoute candidate, final CalculatedRoute root) {
		if (candidate.getDestination().isDepot()) {
			return true;
		}

		BiFunction<LocalDateTime, Double, LocalDateTime> sum = (deliveryTime, elapsedTime) -> deliveryTime.plus(elapsedTime.longValue(), ChronoField.MILLI_OF_DAY.getBaseUnit());

		LocalDateTime deliveryTime = sum.apply(root.getDelivery(), candidate.getTravelTime());
		LocalDateTime deliveryTimeLimit = sum.apply(candidate.getLastDelivered().getDelivery(), config.getRoute().getLateDeliveryMax());

		return !deliveryTime.isAfter(deliveryTimeLimit);
	}

	private boolean checkExistence(final CalculatedRoute candidate, final CalculatedRoute root, final CalculatedRouteMap routeMap) {
		return !routeMap.getRoutes().contains(routeSimulator.simulate(root, candidate));
	}

	private boolean checkPickTime(final CalculatedRoute candidate, final CalculatedRoute root) {
		return candidate.getDestination().isDepot() || !root.getCurrentDepotArrivalTime().isBefore(candidate.getPickup());
	}

	private boolean checkUniqueVisit(final CalculatedRoute candidate, final CalculatedRoute root) {
		return candidate.getDestination().isDepot() || !root.getOrderedRoutes().contains(candidate.getLastDelivered());
	}

	private boolean checkDepotWithoutTarget(final CalculatedRoute candidate, final CalculatedRoute root, final CalculatedRouteMap routeMap) {
		if (!candidate.getDestination().isDepot()) {
			return true;
		}

		return routeMap.get(candidate.getDestination()).stream().filter(r -> check(r, routeSimulator.simulate(root, candidate), routeMap)).count() > 0;
	}

}
