package com.ifood.logistic.microservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ifood.logistic.microservice.domain.entity.Customer;
import com.ifood.logistic.microservice.exception.CustomerNotFoundException;
import com.ifood.logistic.microservice.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	public Customer create(final Customer customer) {
		return customerRepository.save(customer);
	}

	public Customer update(final Customer customer) {
		if (!customerRepository.existsById(customer.getId())) {
			throw new CustomerNotFoundException();
		}

		return customerRepository.save(customer);
	}

	public Page<Customer> search(final Pageable pageable) {
		return customerRepository.findAll(pageable);
	}

	public Customer get(final Long id) {
		return customerRepository.findById(id).orElseThrow(CustomerNotFoundException::new);
	}

}
