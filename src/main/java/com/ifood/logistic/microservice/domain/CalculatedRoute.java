package com.ifood.logistic.microservice.domain;

import java.time.LocalDateTime;
import java.util.List;

public interface CalculatedRoute extends AlternativeRoute {

	Double getTravelTime();

	Double getLateDeliveryTime();

	List<Location> getLocations();

	Location getCurrentDepot();

	LocalDateTime getCurrentDepotArrivalTime();

	OrderedRoute getLastDelivered();

	List<SubRoute> getSubRoutes();

	SubRoute getPendingSubRoute();

	void setLateDeliveryTime(Double lateDeliveryTime);

	void setPendingSubRoute(SubRoute pending);

	default int getNumberOfOrderedRoutes() {
		return getOrderedRoutes().size();
	}

	default int getNumberOfLocations() {
		return getLocations().size();
	}

}
