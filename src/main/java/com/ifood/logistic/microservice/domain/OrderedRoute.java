package com.ifood.logistic.microservice.domain;

public interface OrderedRoute extends Route {

	@Override
	default boolean isAlternative() {
		return false;
	}
}
