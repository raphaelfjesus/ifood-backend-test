package com.ifood.logistic.microservice.domain.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity<I extends Serializable> {

	@Id
	@GeneratedValue
	protected I id;

}
