package com.ifood.logistic.microservice.domain;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.Getter;

public class CalculatedRouteMap {

	private Map<Location, Map<Location, Set<CalculatedRoute>>> map;
	private Map<Location, Set<CalculatedRoute>> routesByOriginMap;

	@Getter
	private Set<CalculatedRoute> routes;

	@Getter
	private Set<OrderedRoute> orderedRoutes;

	public CalculatedRouteMap() {
		map = new HashMap<>();
		routesByOriginMap = new HashMap<>();

		routes = new HashSet<>();
		orderedRoutes = new HashSet<>();
	}

	public void putIfAbsent(final CalculatedRoute route) {
		if (!get(route.getOrigin(), route.getDestination()).contains(route)) {
			get(route.getOrigin(), route.getDestination()).add(route);
			get(route.getOrigin()).add(route);

			routes.add(route);

			if (route.getOrigin().isDepot()) {
				orderedRoutes.addAll(route.getOrderedRoutes());
			}
		}
	}

	public Set<CalculatedRoute> get(final Location origin) {
		return routesByOriginMap.computeIfAbsent(origin, k -> new HashSet<>());
	}

	private Set<CalculatedRoute> get(final Location origin, final Location destination) {
		return get(map, origin, destination);
	}

	private Set<CalculatedRoute> get(final Map<Location, Map<Location, Set<CalculatedRoute>>> map, final Location origin, final Location destination) {
		Set<CalculatedRoute> calculatedRoutes = get(map, origin).get(destination);

		if (calculatedRoutes == null) {
			calculatedRoutes = new HashSet<>();
			get(map, origin).put(origin, calculatedRoutes);
		}

		return calculatedRoutes;
	}

	private Map<Location, Set<CalculatedRoute>> get(final Map<Location, Map<Location, Set<CalculatedRoute>>> map, final Location origin) {
		return map.computeIfAbsent(origin, k -> new HashMap<>());
	}

}
