package com.ifood.logistic.microservice.domain.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import io.swagger.annotations.ApiModel;

@Getter
@Setter
@ApiModel(value = "Route", description = "Object route.")
public class RouteDTO {

	private Long id;
	private List<Long> orders;

	public RouteDTO(Long id, List<Long> orders) {
		super();
		setId(id);
		setOrders(orders);
	}

}
