package com.ifood.logistic.microservice.domain;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(of = { "origin", "destination", "pickup", "delivery" })
public class AlternativeRouteImpl implements AlternativeRoute {

	private Location origin;
	private Location destination;
	private LocalDateTime pickup;
	private LocalDateTime delivery;
	private OrderedRoute orderedRoute;

	public AlternativeRouteImpl(Location origin, Location destination) {
		this.origin = origin;
		this.destination = destination;
		pickup = LocalDateTime.MIN;
		delivery = LocalDateTime.MAX;
	}

	public AlternativeRouteImpl(Location origin, OrderedRoute orderedRoute) {
		this.origin = origin;
		destination = orderedRoute.getDestination();
		pickup = orderedRoute.getPickup();
		delivery = orderedRoute.getDelivery();
		this.orderedRoute = orderedRoute;
	}

	@Override
	public List<OrderedRoute> getOrderedRoutes() {
		return orderedRoute != null ? Collections.singletonList(orderedRoute) : Collections.emptyList();
	}

}
