package com.ifood.logistic.microservice.domain;

public interface Location {

	Double getLat();

	Double getLon();

	default boolean isDepot() {
		return false;
	}

	default boolean isNotDepot() {
		return !isDepot();
	}

}
