package com.ifood.logistic.microservice.domain;

import java.util.List;

public interface SubRoute {

	Location getDepot();

	List<OrderedRoute> getOrderedRoutes();

	void addOrderedRoute(OrderedRoute route);

}
