package com.ifood.logistic.microservice.domain.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "RestaurantDTO", description = "Object restaurant.")
public class RestaurantDTO {

	@ApiModelProperty(value = "Unique identifier", accessMode = AccessMode.READ_ONLY)
	private Long id;

	@NotNull
	@ApiModelProperty(value = "Latitude for restaurant location", example = "0.0", required = true)
	private Double lat;

	@NotNull
	@ApiModelProperty(value = "Longitude for restaurant location", example = "0.0", required = true)
	private Double lon;

	public RestaurantDTO(Double lat, Double lon) {
		super();
		this.lat = lat;
		this.lon = lon;
	}

}
