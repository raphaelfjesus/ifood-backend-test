package com.ifood.logistic.microservice.domain.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@ApiModel(value = "OrderDTO", description = "Object order.")
public class OrderDTO {

	@ApiModelProperty(value = "Unique identifier", example = "1", accessMode = AccessMode.READ_ONLY)
	private Long id;

	@NotNull
	@ApiModelProperty(value = "Restaurant id previously registered", example = "1", required = true)
	private Long restaurantId;

	@NotNull
	@ApiModelProperty(value = "Customer id previously registered", example = "1", required = true)
	private Long customerId;

	@NotNull
	@ApiModelProperty(value = "Pickup date of the order", example = "2018-12-27T13:37:00Z", required = true)
	private LocalDateTime pickup;

	@NotNull
	@ApiModelProperty(value = "Delivery date of the order", example = "2018-12-27T13:54:00Z", required = true)
	private LocalDateTime delivery;

	public OrderDTO(Long restaurantId, Long customerId, LocalDateTime pickup, LocalDateTime delivery) {
		super();
		this.restaurantId = restaurantId;
		this.customerId = customerId;
		this.pickup = pickup;
		this.delivery = delivery;
	}

}
