package com.ifood.logistic.microservice.domain;

import java.time.LocalDateTime;

public interface Route {

	Location getOrigin();

	Location getDestination();

	LocalDateTime getPickup();

	LocalDateTime getDelivery();

	boolean isAlternative();

}
