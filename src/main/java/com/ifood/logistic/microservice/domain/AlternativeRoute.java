package com.ifood.logistic.microservice.domain;

import java.util.List;

public interface AlternativeRoute extends Route {

	List<OrderedRoute> getOrderedRoutes();

	@Override
	default boolean isAlternative() {
		return true;
	}
}
