package com.ifood.logistic.microservice.domain.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import com.ifood.logistic.microservice.domain.Location;
import com.ifood.logistic.microservice.domain.OrderedRoute;

@Getter
@Setter
@Entity
@Table(name = "PURCHASE_ORDER")
public class Order extends AbstractEntity<Long> implements OrderedRoute {

	@NotNull
	@ManyToOne
	private Restaurant restaurant;

	@NotNull
	@ManyToOne
	private Customer customer;

	@NotNull
	private LocalDateTime pickup;

	@NotNull
	private LocalDateTime delivery;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST }, mappedBy = "orders")
	private List<Route> routes;

	@Override
	public Location getOrigin() {
		return restaurant;
	}

	@Override
	public Location getDestination() {
		return customer;
	}

}
