package com.ifood.logistic.microservice.domain;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "origin", "destination", "pickup", "delivery", "locations" })
public class CalculatedRouteImpl implements CalculatedRoute {

	private Location origin;
	private Location destination;
	private Location currentDepot;
	private List<Location> locations;

	private LocalDateTime pickup;
	private LocalDateTime delivery;
	private LocalDateTime currentDepotArrivalTime;

	private Double travelTime;
	private Double lateDeliveryTime;

	private List<OrderedRoute> orderedRoutes;
	private OrderedRoute lastDelivered;

	private List<SubRoute> subRoutes;
	private SubRoute pendingSubRoute;

	public CalculatedRouteImpl() {
		locations = new LinkedList<>();
		subRoutes = new LinkedList<>();
		orderedRoutes = new LinkedList<>();

		travelTime = Double.valueOf(0);
		lateDeliveryTime = Double.valueOf(0);
	}

	public void setOrigin(Location origin) {
		this.origin = origin;
		locations.add(0, origin);
	}

	public void setDestination(Location destination) {
		this.destination = destination;
		locations.add(destination);
	}

	public void setPickup(LocalDateTime pickup) {
		this.pickup = pickup.truncatedTo(ChronoUnit.MINUTES);
	}

	public void setDelivery(LocalDateTime delivery) {
		this.delivery = delivery.truncatedTo(ChronoUnit.MINUTES);
	}

	public void setOrderedRoutes(List<OrderedRoute> orderedRoutes) {
		this.orderedRoutes = orderedRoutes;

		if (!orderedRoutes.isEmpty()) {
			lastDelivered = orderedRoutes.get(0);
		}
	}

	public void addSubRoute(SubRoute subRoute) {
		pendingSubRoute = subRoute;
		subRoutes.add(subRoute);

		if (lastDelivered != null) {
			subRoute.addOrderedRoute(lastDelivered);
		}
	}

}
