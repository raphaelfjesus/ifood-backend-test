package com.ifood.logistic.microservice.domain.entity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.ifood.logistic.microservice.domain.Location;
import com.ifood.logistic.microservice.domain.OrderedRoute;
import com.ifood.logistic.microservice.domain.SubRoute;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Route extends AbstractEntity<Long> implements SubRoute {

	@NotNull
	@ManyToOne
	private Restaurant restaurant;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@JoinTable(name = "ROUTE_PURCHASE_ORDER", joinColumns = { @JoinColumn(name = "ROUTE_ID") }, inverseJoinColumns = { @JoinColumn(name = "PURCHASE_ORDER_ID") })
	private List<Order> orders = new LinkedList<>();

	public Route(@NotNull Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	@Override
	public Location getDepot() {
		return restaurant;
	}

	@Override
	public List<OrderedRoute> getOrderedRoutes() {
		return Collections.unmodifiableList(orders);
	}

	@Override
	public void addOrderedRoute(OrderedRoute orderedRoute) {
		orders.add((Order) orderedRoute);
	}

}
