package com.ifood.logistic.microservice.domain.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import com.ifood.logistic.microservice.domain.Location;

@Getter
@Setter
@Entity
public class Restaurant extends AbstractEntity<Long> implements Location {

	@NotNull
	private Double lat;

	@NotNull
	private Double lon;

	@Override
	public boolean isDepot() {
		return true;
	}

}
