package com.ifood.logistic.microservice.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ifood.logistic.microservice.domain.dto.ListRouteDTO;
import com.ifood.logistic.microservice.domain.dto.RouteDTO;
import com.ifood.logistic.microservice.domain.entity.Order;
import com.ifood.logistic.microservice.domain.entity.Route;
import com.ifood.logistic.microservice.service.RouteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/routes", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
@Api(tags = { "Routes" })
public class RouteController {

	@Autowired
	private RouteService routeService;

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retrieve a list of routes")
	public ListRouteDTO getAll() {
		return convertToDTO(routeService.getAll());
	}

	private ListRouteDTO convertToDTO(final List<Route> routes) {
		List<RouteDTO> routesDTO = routes.stream() //
				.map(r -> new RouteDTO(r.getId(), r.getOrders().stream().map(Order::getId).collect(Collectors.toList()))) //
				.collect(Collectors.toList());

		return new ListRouteDTO(routesDTO);
	}

	@GetMapping(path = "/plotted", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retrieves the plotted routes in the Cartesian plane")
	public byte[] draw() throws IOException {
		return routeService.draw();
	}

}
