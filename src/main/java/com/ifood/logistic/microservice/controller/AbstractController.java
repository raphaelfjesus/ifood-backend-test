package com.ifood.logistic.microservice.controller;

import java.io.Serializable;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import com.ifood.logistic.microservice.domain.entity.AbstractEntity;

class AbstractController<I extends Serializable, E extends AbstractEntity<I>, D> {

	@Autowired
	private ModelMapper modelMapper;

	protected Page<D> convertToDTO(final Page<E> page, final Class<D> dtoClass) {
		return new PageImpl<>(page.getContent().stream().map(e -> convertToDTO(e, dtoClass)).collect(Collectors.toList()), page.getPageable(), page.getTotalElements());
	}

	protected D convertToDTO(final E entity, final Class<D> dtoClass) {
		return modelMapper.map(entity, dtoClass);
	}

	protected E convertToEntity(final D dto, final Class<E> entityClass) {
		return modelMapper.map(dto, entityClass);
	}

	protected E convertToEntity(final D dto, final I id, final Class<E> entityClass) {
		E entity = modelMapper.map(dto, entityClass);
		entity.setId(id);

		return entity;
	}

}
