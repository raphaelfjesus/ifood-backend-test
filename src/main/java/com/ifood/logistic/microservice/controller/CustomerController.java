package com.ifood.logistic.microservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ifood.logistic.microservice.domain.dto.CustomerDTO;
import com.ifood.logistic.microservice.domain.entity.Customer;
import com.ifood.logistic.microservice.service.CustomerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(path = "/customers", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
@Api(tags = { "Customers" })
public class CustomerController extends AbstractController<Long, Customer, CustomerDTO> {

	@Autowired
	private CustomerService customerService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a new customer", response = Long.class)
	public Long create(@RequestBody @Valid final CustomerDTO customer) {
		return customerService.create(convertToEntity(customer, Customer.class)).getId();
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Update the customer by id")
	public void update(@RequestBody @Valid final CustomerDTO customer, @PathVariable final Long id) {
		customerService.update(convertToEntity(customer, id, Customer.class));
	}

	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retrieve the customer by id")
	public CustomerDTO get(@ApiParam(value = "Unique identifier", required = true) @PathVariable final Long id) {
		return convertToDTO(customerService.get(id), CustomerDTO.class);
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retrieve a list of paged customers")
	@ApiImplicitParams({ //
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Results page to retrieve (0..N)"), //
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Number of customers per page") //
	})
	public Page<CustomerDTO> search(final @ApiIgnore Pageable pageable) {
		return convertToDTO(customerService.search(pageable), CustomerDTO.class);
	}

}
