package com.ifood.logistic.microservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ifood.logistic.microservice.domain.dto.RestaurantDTO;
import com.ifood.logistic.microservice.domain.entity.Restaurant;
import com.ifood.logistic.microservice.service.RestaurantService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(path = "/restaurants", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
@Api(tags = { "Restaurants" })
public class RestaurantController extends AbstractController<Long, Restaurant, RestaurantDTO> {

	@Autowired
	private RestaurantService restaurantService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a new restaurant")
	public Long create(@RequestBody @Valid final RestaurantDTO restaurant) {
		return restaurantService.create(convertToEntity(restaurant, Restaurant.class)).getId();
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Update the restaurant by id")
	public void update(@RequestBody @Valid final RestaurantDTO restaurant, @PathVariable final Long id) {
		restaurantService.update(convertToEntity(restaurant, id, Restaurant.class));
	}

	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retrieve the restaurant by id")
	public RestaurantDTO get(@ApiParam(value = "Unique identifier", required = true) @PathVariable final Long id) {
		return convertToDTO(restaurantService.get(id), RestaurantDTO.class);
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retrieve a list of paged restaurants")
	@ApiImplicitParams({ //
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Results page to retrieve (0..N)"), //
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Number of restaurants per page") //
	})
	public Page<RestaurantDTO> search(final @ApiIgnore Pageable pageable) {
		return convertToDTO(restaurantService.search(pageable), RestaurantDTO.class);
	}

}
