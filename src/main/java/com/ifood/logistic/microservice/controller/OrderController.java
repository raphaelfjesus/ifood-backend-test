package com.ifood.logistic.microservice.controller;

import java.time.LocalDateTime;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ifood.logistic.microservice.domain.dto.OrderDTO;
import com.ifood.logistic.microservice.domain.entity.Order;
import com.ifood.logistic.microservice.service.CustomerService;
import com.ifood.logistic.microservice.service.OrderService;
import com.ifood.logistic.microservice.service.RestaurantService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(path = "/orders", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
@Api(tags = { "Orders" })
public class OrderController extends AbstractController<Long, Order, OrderDTO> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private RestaurantService restaurantService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a new order")
	public Long create(@RequestBody @Valid final OrderDTO order) {
		return orderService.create(convertToEntity(order, Order.class)).getId();
	}

	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retrieve the order by id")
	public OrderDTO get(@ApiParam(value = "Unique identifier", required = true) @PathVariable final Long id) {
		return convertToDTO(orderService.get(id), OrderDTO.class);
	}

	@GetMapping(params = "restaurantId")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retrieve a list of paged orders")
	@ApiImplicitParams({ //
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Results page to retrieve (0..N)"), //
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Number of orders per page") //
	})
	public Page<OrderDTO> search(@RequestParam(required = true) Long restaurantId, @RequestParam(required = false) LocalDateTime deliveryStart,
			@RequestParam(required = false) LocalDateTime deliveryEnd, @ApiIgnore Pageable pageable) {
		return convertToDTO(orderService.search(restaurantId, deliveryStart, deliveryEnd, pageable), OrderDTO.class);
	}

	@Override
	protected Order convertToEntity(final OrderDTO dto, final Class<Order> entityClass) {
		Order order = super.convertToEntity(dto, entityClass);
		order.setRestaurant(restaurantService.get(dto.getRestaurantId()));
		order.setCustomer(customerService.get(dto.getCustomerId()));

		return order;
	}

	@Override
	protected OrderDTO convertToDTO(final Order entity, final Class<OrderDTO> dtoClass) {
		OrderDTO order = super.convertToDTO(entity, dtoClass);
		order.setRestaurantId(entity.getRestaurant().getId());
		order.setCustomerId(entity.getCustomer().getId());

		return order;
	}

}
