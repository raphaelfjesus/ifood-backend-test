package com.ifood.logistic.microservice.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ifood.logistic.microservice.domain.dto.CustomerDTO;
import com.ifood.logistic.microservice.domain.dto.ListRouteDTO;
import com.ifood.logistic.microservice.domain.dto.OrderDTO;
import com.ifood.logistic.microservice.domain.dto.RestaurantDTO;
import com.ifood.logistic.microservice.domain.dto.RouteDTO;
import com.ifood.logistic.microservice.repository.CustomerRepository;
import com.ifood.logistic.microservice.repository.OrderRepository;
import com.ifood.logistic.microservice.repository.RestaurantRepository;
import com.ifood.logistic.microservice.repository.RouteRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RouteControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private RouteRepository routeRepository;

	@Autowired
	private RestaurantRepository restaurantRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Test
	public void testGetAll() {
		routeRepository.deleteAllInBatch();
		orderRepository.deleteAllInBatch();
		customerRepository.deleteAllInBatch();
		restaurantRepository.deleteAllInBatch();

		List<OrderDTO> orders = createOrders();

		ResponseEntity<ListRouteDTO> responseGet = restTemplate.getForEntity("/routes", ListRouteDTO.class);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertNotNull(responseGet.getBody().getRoutes());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));

		assertEquals(2, responseGet.getBody().getRoutes().size());
		assertEquals(orders.size(), (int) responseGet.getBody().getRoutes().stream() //
				.map(RouteDTO::getOrders) //
				.flatMap(Collection::stream) //
				.collect(Collectors.toList()) //
				.stream() //
				.count());
	}

	@Test
	public void testGetAllWhenNoResult() {
		routeRepository.deleteAllInBatch();
		orderRepository.deleteAllInBatch();

		ResponseEntity<ListRouteDTO> responseGet = restTemplate.getForEntity("/routes", ListRouteDTO.class);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertNotNull(responseGet.getBody().getRoutes());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(0, responseGet.getBody().getRoutes().size());
	}

	@Test
	public void testGetPlottedRoutes() {
		routeRepository.deleteAllInBatch();
		orderRepository.deleteAllInBatch();
		customerRepository.deleteAllInBatch();
		restaurantRepository.deleteAllInBatch();

		createOrders();

		ResponseEntity<byte[]> responseGet = restTemplate.getForEntity("/routes/plotted", byte[].class);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
	}

	@Test
	public void testGetPlottedRoutesWhenNoResult() {
		routeRepository.deleteAllInBatch();
		orderRepository.deleteAllInBatch();
		customerRepository.deleteAllInBatch();
		restaurantRepository.deleteAllInBatch();

		ResponseEntity<Void> responseGet = restTemplate.getForEntity("/routes", Void.class);

		assertNotNull(responseGet);
		assertNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
	}

	private List<OrderDTO> createOrders() {
		RestaurantDTO restaurant1 = createRestaurant(0.0, 0.0);
		RestaurantDTO restaurant2 = createRestaurant(0.0, 1.0);

		CustomerDTO customer1 = createCustomer(1.0, 0.0);
		CustomerDTO customer2 = createCustomer(1.0, 1.0);
		CustomerDTO customer3 = createCustomer(2.0, 0.0);
		CustomerDTO customer4 = createCustomer(2.0, 2.0);
		CustomerDTO customer5 = createCustomer(1.0, 2.0);

		LocalDateTime now = LocalDateTime.parse("2018-12-26 00:00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

		List<OrderDTO> orders = new ArrayList<>();
		orders.add(new OrderDTO(restaurant1.getId(), customer1.getId(), now, now.plusDays(1)));
		orders.add(new OrderDTO(restaurant1.getId(), customer2.getId(), now, now.plusDays(1)));
		orders.add(new OrderDTO(restaurant1.getId(), customer3.getId(), now, now.plusDays(1)));
		orders.add(new OrderDTO(restaurant2.getId(), customer4.getId(), now, now.plusDays(1)));
		orders.add(new OrderDTO(restaurant2.getId(), customer5.getId(), now, now.plusDays(1)));

		orders.forEach((order) -> {
			ResponseEntity<Long> responsePost = restTemplate.postForEntity("/orders", order, Long.class);

			assertNotNull(responsePost);
			assertNotNull(responsePost.getBody());
			assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));
		});

		return orders;
	}

	private RestaurantDTO createRestaurant(final double lat, final double lon) {
		RestaurantDTO restaurant = new RestaurantDTO(lat, lon);

		ResponseEntity<Long> response = restTemplate.postForEntity("/restaurants", restaurant, Long.class);

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

		restaurant.setId(response.getBody());

		return restaurant;
	}

	private CustomerDTO createCustomer(final double lat, final double lon) {
		CustomerDTO customer = new CustomerDTO(lat, lon);

		ResponseEntity<Long> response = restTemplate.postForEntity("/customers", customer, Long.class);

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

		customer.setId(response.getBody());

		return customer;
	}

}
