package com.ifood.logistic.microservice.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ifood.logistic.microservice.domain.RestResponsePage;
import com.ifood.logistic.microservice.domain.ErrorResponse;
import com.ifood.logistic.microservice.domain.FieldError;
import com.ifood.logistic.microservice.domain.dto.RestaurantDTO;
import com.ifood.logistic.microservice.repository.OrderRepository;
import com.ifood.logistic.microservice.repository.RestaurantRepository;
import com.ifood.logistic.microservice.repository.RouteRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestaurantControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private RestaurantRepository restaurantRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private RouteRepository routeRepository;

	@Test
	public void testCreate() {
		RestaurantDTO restaurant = new RestaurantDTO(1.1, 1.1);

		ResponseEntity<Long> response = restTemplate.postForEntity("/restaurants", restaurant, Long.class);

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));
	}

	@Test
	public void testCreateWhenBadRequest() {
		ResponseEntity<ErrorResponse> response = restTemplate.postForEntity("/restaurants", new RestaurantDTO(), ErrorResponse.class);

		FieldError expectedLatField = new FieldError("lat", "não pode ser nulo");
		FieldError expectedLonField = new FieldError("lon", "não pode ser nulo");

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
		assertThat(response.getBody().getMessage(), equalTo("Bad Request"));
		assertThat(response.getBody().getDetails(), equalTo("uri=/restaurants"));
		assertThat(response.getBody().getTimestamp(), not(nullValue()));
		assertThat(response.getBody().getFieldErros(), hasSize(2));
		assertThat(response.getBody().getFieldErros(), hasItems(expectedLatField, expectedLonField));
	}

	@Test
	public void testUpdate() {
		RestaurantDTO restaurant = new RestaurantDTO(1.1, 1.1);

		ResponseEntity<Long> responsePost = restTemplate.postForEntity("/restaurants", restaurant, Long.class);

		assertNotNull(responsePost);
		assertNotNull(responsePost.getBody());
		assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));

		restaurant.setLat(2.2);
		restaurant.setLon(2.2);

		HttpEntity<RestaurantDTO> requestEntity = new HttpEntity<>(restaurant);
		ResponseEntity<Void> responsePut = restTemplate.exchange("/restaurants/" + responsePost.getBody(), HttpMethod.PUT, requestEntity, Void.class);

		assertNotNull(responsePut);
		assertNull(responsePut.getBody());
		assertThat(responsePut.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

		ResponseEntity<RestaurantDTO> responseGet = restTemplate.getForEntity("/restaurants/" + responsePost.getBody(), RestaurantDTO.class);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(responsePost.getBody(), responseGet.getBody().getId());
		assertEquals(restaurant.getLat(), responseGet.getBody().getLat());
		assertEquals(restaurant.getLon(), responseGet.getBody().getLon());
	}

	@Test
	public void testUpdateWhenBadRequest() {
		RestaurantDTO restaurant = createRestaurant();

		HttpEntity<RestaurantDTO> requestEntity = new HttpEntity<>(new RestaurantDTO());
		ResponseEntity<ErrorResponse> response = restTemplate.exchange("/restaurants/" + restaurant.getId(), HttpMethod.PUT, requestEntity, ErrorResponse.class);

		FieldError expectedLatField = new FieldError("lat", "não pode ser nulo");
		FieldError expectedLonField = new FieldError("lon", "não pode ser nulo");

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
		assertThat(response.getBody().getMessage(), equalTo("Bad Request"));
		assertThat(response.getBody().getDetails(), equalTo("uri=/restaurants/" + restaurant.getId()));
		assertThat(response.getBody().getTimestamp(), not(nullValue()));
		assertThat(response.getBody().getFieldErros(), hasSize(2));
		assertThat(response.getBody().getFieldErros(), hasItems(expectedLatField, expectedLonField));
	}

	@Test
	public void testUpdateWhenRecordNotFound() {
		RestaurantDTO restaurant = new RestaurantDTO(1.1, 1.1);

		HttpEntity<RestaurantDTO> requestEntity = new HttpEntity<>(restaurant);
		ResponseEntity<Void> responsePut = restTemplate.exchange("/restaurants/" + System.currentTimeMillis(), HttpMethod.PUT, requestEntity, Void.class);

		assertNotNull(responsePut);
		assertNull(responsePut.getBody());
		assertThat(responsePut.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
	}

	@Test
	public void testGetById() {
		RestaurantDTO restaurant = createRestaurant();

		ResponseEntity<RestaurantDTO> responseGet = restTemplate.getForEntity("/restaurants/" + restaurant.getId(), RestaurantDTO.class);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(restaurant.getId(), responseGet.getBody().getId());
		assertEquals(restaurant.getLat(), responseGet.getBody().getLat());
		assertEquals(restaurant.getLon(), responseGet.getBody().getLon());
	}

	@Test
	public void testGetByIdWhenRecordNotFound() {
		ResponseEntity<Void> responseGet = restTemplate.getForEntity("/restaurants/" + System.currentTimeMillis(), Void.class);

		assertNotNull(responseGet);
		assertNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
	}

	@Test
	public void testSearch() {
		routeRepository.deleteAllInBatch();
		orderRepository.deleteAllInBatch();
		restaurantRepository.deleteAllInBatch();

		List<RestaurantDTO> restaurants = new ArrayList<>();
		restaurants.add(new RestaurantDTO(1.1, 1.1));
		restaurants.add(new RestaurantDTO(2.2, 2.2));
		restaurants.add(new RestaurantDTO(3.3, 3.3));

		restaurants.forEach((restaurant) -> {
			ResponseEntity<Long> responsePost = restTemplate.postForEntity("/restaurants", restaurant, Long.class);

			assertNotNull(responsePost);
			assertNotNull(responsePost.getBody());
			assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));
		});

		Pageable pageable = PageRequest.of(0, 2);

		ParameterizedTypeReference<RestResponsePage<RestaurantDTO>> responseType = new ParameterizedTypeReference<RestResponsePage<RestaurantDTO>>() {};
		String url = String.format("/restaurants?page=%d&size=%d", pageable.getPageNumber(), pageable.getPageSize());

		ResponseEntity<RestResponsePage<RestaurantDTO>> responseGet = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(restaurants.size(), responseGet.getBody().getTotalElements());
		assertEquals(pageable.getPageSize(), responseGet.getBody().getContent().size());
	}

	@Test
	public void testSearchWhenNoResult() {
		routeRepository.deleteAllInBatch();
		orderRepository.deleteAllInBatch();
		restaurantRepository.deleteAllInBatch();

		Pageable pageable = PageRequest.of(0, 2);

		ParameterizedTypeReference<RestResponsePage<RestaurantDTO>> responseType = new ParameterizedTypeReference<RestResponsePage<RestaurantDTO>>() {};
		String url = String.format("/restaurants?page=%d&size=%d", pageable.getPageNumber(), pageable.getPageSize());

		ResponseEntity<RestResponsePage<RestaurantDTO>> responseGet = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(0, responseGet.getBody().getTotalElements());
		assertEquals(0, responseGet.getBody().getContent().size());
	}

	private RestaurantDTO createRestaurant() {
		RestaurantDTO restaurant = new RestaurantDTO(1.1, 1.1);

		ResponseEntity<Long> responsePost = restTemplate.postForEntity("/restaurants", restaurant, Long.class);

		assertNotNull(responsePost);
		assertNotNull(responsePost.getBody());
		assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));

		restaurant.setId(responsePost.getBody());

		return restaurant;
	}

}
