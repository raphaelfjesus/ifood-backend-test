package com.ifood.logistic.microservice.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ifood.logistic.microservice.domain.RestResponsePage;
import com.ifood.logistic.microservice.domain.ErrorResponse;
import com.ifood.logistic.microservice.domain.FieldError;
import com.ifood.logistic.microservice.domain.dto.CustomerDTO;
import com.ifood.logistic.microservice.repository.CustomerRepository;
import com.ifood.logistic.microservice.repository.OrderRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Test
	public void testCreate() {
		CustomerDTO customer = new CustomerDTO(1.1, 1.1);

		ResponseEntity<Long> response = restTemplate.postForEntity("/customers", customer, Long.class);

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));
	}

	@Test
	public void testCreateWhenBadRequest() {
		ResponseEntity<ErrorResponse> response = restTemplate.postForEntity("/customers", new CustomerDTO(), ErrorResponse.class);

		FieldError expectedLatField = new FieldError("lat", "não pode ser nulo");
		FieldError expectedLonField = new FieldError("lon", "não pode ser nulo");

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
		assertThat(response.getBody().getMessage(), equalTo("Bad Request"));
		assertThat(response.getBody().getDetails(), equalTo("uri=/customers"));
		assertThat(response.getBody().getTimestamp(), not(nullValue()));
		assertThat(response.getBody().getFieldErros(), hasSize(2));
		assertThat(response.getBody().getFieldErros(), hasItems(expectedLatField, expectedLonField));
	}

	@Test
	public void testUpdate() {
		CustomerDTO customer = createCustomer();
		customer.setLat(2.2);
		customer.setLon(2.2);

		HttpEntity<CustomerDTO> requestEntity = new HttpEntity<>(customer);
		ResponseEntity<Void> responsePut = restTemplate.exchange("/customers/" + customer.getId(), HttpMethod.PUT, requestEntity, Void.class);

		assertNotNull(responsePut);
		assertNull(responsePut.getBody());
		assertThat(responsePut.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));

		ResponseEntity<CustomerDTO> responseGet = restTemplate.getForEntity("/customers/" + customer.getId(), CustomerDTO.class);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(customer.getId(), responseGet.getBody().getId());
		assertEquals(customer.getLat(), responseGet.getBody().getLat());
		assertEquals(customer.getLon(), responseGet.getBody().getLon());
	}

	@Test
	public void testUpdateWhenBadRequest() {
		CustomerDTO customer = createCustomer();

		HttpEntity<CustomerDTO> requestEntity = new HttpEntity<>(new CustomerDTO());
		ResponseEntity<ErrorResponse> response = restTemplate.exchange("/customers/" + customer.getId(), HttpMethod.PUT, requestEntity, ErrorResponse.class);

		FieldError expectedLatField = new FieldError("lat", "não pode ser nulo");
		FieldError expectedLonField = new FieldError("lon", "não pode ser nulo");

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
		assertThat(response.getBody().getMessage(), equalTo("Bad Request"));
		assertThat(response.getBody().getDetails(), equalTo("uri=/customers/" + customer.getId()));
		assertThat(response.getBody().getTimestamp(), not(nullValue()));
		assertThat(response.getBody().getFieldErros(), hasSize(2));
		assertThat(response.getBody().getFieldErros(), hasItems(expectedLatField, expectedLonField));
	}

	@Test
	public void testUpdateWhenRecordNotFound() {
		CustomerDTO customer = new CustomerDTO(1.1, 1.1);

		HttpEntity<CustomerDTO> requestEntity = new HttpEntity<>(customer);
		ResponseEntity<Void> responsePut = restTemplate.exchange("/customers/" + System.currentTimeMillis(), HttpMethod.PUT, requestEntity, Void.class);

		assertNotNull(responsePut);
		assertNull(responsePut.getBody());
		assertThat(responsePut.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
	}

	@Test
	public void testGetById() {
		CustomerDTO customer = new CustomerDTO(1.1, 1.1);

		ResponseEntity<Long> responsePost = restTemplate.postForEntity("/customers", customer, Long.class);

		assertNotNull(responsePost);
		assertNotNull(responsePost.getBody());
		assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));

		ResponseEntity<CustomerDTO> responseGet = restTemplate.getForEntity("/customers/" + responsePost.getBody(), CustomerDTO.class);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(responsePost.getBody(), responseGet.getBody().getId());
		assertEquals(customer.getLat(), responseGet.getBody().getLat());
		assertEquals(customer.getLon(), responseGet.getBody().getLon());
	}

	@Test
	public void testGetByIdWhenRecordNotFound() {
		ResponseEntity<Void> responseGet = restTemplate.getForEntity("/customers/" + System.currentTimeMillis(), Void.class);

		assertNotNull(responseGet);
		assertNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
	}

	@Test
	public void testSearch() {
		orderRepository.deleteAllInBatch();
		customerRepository.deleteAllInBatch();

		List<CustomerDTO> customers = new ArrayList<>();
		customers.add(new CustomerDTO(1.1, 1.1));
		customers.add(new CustomerDTO(2.2, 2.2));
		customers.add(new CustomerDTO(3.3, 3.3));

		customers.forEach((customer) -> {
			ResponseEntity<Long> responsePost = restTemplate.postForEntity("/customers", customer, Long.class);

			assertNotNull(responsePost);
			assertNotNull(responsePost.getBody());
			assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));
		});

		Pageable pageable = PageRequest.of(0, 2);

		ParameterizedTypeReference<RestResponsePage<CustomerDTO>> responseType = new ParameterizedTypeReference<RestResponsePage<CustomerDTO>>() {};
		String url = String.format("/customers?page=%d&size=%d", pageable.getPageNumber(), pageable.getPageSize());

		ResponseEntity<RestResponsePage<CustomerDTO>> responseGet = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(customers.size(), responseGet.getBody().getTotalElements());
		assertEquals(pageable.getPageSize(), responseGet.getBody().getContent().size());
	}

	@Test
	public void testSearchWhenNoResult() {
		orderRepository.deleteAllInBatch();
		customerRepository.deleteAllInBatch();

		Pageable pageable = PageRequest.of(0, 2);

		ParameterizedTypeReference<RestResponsePage<CustomerDTO>> responseType = new ParameterizedTypeReference<RestResponsePage<CustomerDTO>>() {};
		String url = String.format("/customers?page=%d&size=%d", pageable.getPageNumber(), pageable.getPageSize());

		ResponseEntity<RestResponsePage<CustomerDTO>> responseGet = restTemplate.exchange(url, HttpMethod.GET, null, responseType);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(0, responseGet.getBody().getTotalElements());
		assertEquals(0, responseGet.getBody().getContent().size());
	}

	private CustomerDTO createCustomer() {
		CustomerDTO customer = new CustomerDTO(1.1, 1.1);

		ResponseEntity<Long> responsePost = restTemplate.postForEntity("/customers", customer, Long.class);

		assertNotNull(responsePost);
		assertNotNull(responsePost.getBody());
		assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));

		customer.setId(responsePost.getBody());

		return customer;
	}

}
