package com.ifood.logistic.microservice.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ifood.logistic.microservice.domain.ErrorResponse;
import com.ifood.logistic.microservice.domain.FieldError;
import com.ifood.logistic.microservice.domain.RestResponsePage;
import com.ifood.logistic.microservice.domain.dto.CustomerDTO;
import com.ifood.logistic.microservice.domain.dto.OrderDTO;
import com.ifood.logistic.microservice.domain.dto.RestaurantDTO;
import com.ifood.logistic.microservice.repository.OrderRepository;
import com.ifood.logistic.microservice.repository.RouteRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private RouteRepository routeRepository;

	@Test
	public void testCreate() {
		RestaurantDTO restaurant = createRestaurant();
		CustomerDTO customer = createCustomer();

		LocalDateTime now = LocalDateTime.now();

		OrderDTO order = new OrderDTO(restaurant.getId(), customer.getId(), now, now.plusMinutes(10));

		ResponseEntity<Long> response = restTemplate.postForEntity("/orders", order, Long.class);

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));
	}

	@Test
	public void testCreateWhenBadRequest() {
		ResponseEntity<ErrorResponse> response = restTemplate.postForEntity("/orders", new OrderDTO(), ErrorResponse.class);

		FieldError expectedRestaurant = new FieldError("restaurantId", "não pode ser nulo");
		FieldError expectedCustomer = new FieldError("customerId", "não pode ser nulo");
		FieldError expectedPickup = new FieldError("pickup", "não pode ser nulo");
		FieldError expectedDelivery = new FieldError("delivery", "não pode ser nulo");

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
		assertThat(response.getBody().getMessage(), equalTo("Bad Request"));
		assertThat(response.getBody().getDetails(), equalTo("uri=/orders"));
		assertThat(response.getBody().getTimestamp(), not(nullValue()));
		assertThat(response.getBody().getFieldErros(), hasSize(4));
		assertThat(response.getBody().getFieldErros(), hasItems(expectedRestaurant, expectedCustomer, expectedPickup, expectedDelivery));
	}

	@Test
	public void testGetById() {
		RestaurantDTO restaurant = createRestaurant();
		CustomerDTO customer = createCustomer();

		LocalDateTime now = LocalDateTime.now();

		OrderDTO order = new OrderDTO(restaurant.getId(), customer.getId(), now, now.plusMinutes(10));

		ResponseEntity<Long> responsePost = restTemplate.postForEntity("/orders", order, Long.class);

		assertNotNull(responsePost);
		assertNotNull(responsePost.getBody());
		assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));

		ResponseEntity<OrderDTO> responseGet = restTemplate.getForEntity("/orders/" + responsePost.getBody(), OrderDTO.class);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(responsePost.getBody(), responseGet.getBody().getId());
		assertEquals(order.getRestaurantId(), responseGet.getBody().getRestaurantId());
		assertEquals(order.getCustomerId(), responseGet.getBody().getCustomerId());
		assertEquals(order.getPickup(), responseGet.getBody().getPickup());
		assertEquals(order.getDelivery(), responseGet.getBody().getDelivery());
	}

	@Test
	public void testGetByIdWhenRecordNotFound() {
		ResponseEntity<Void> responseGet = restTemplate.getForEntity("/orders/" + System.currentTimeMillis(), Void.class);

		assertNotNull(responseGet);
		assertNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
	}

	@Test
	public void testSearch() {
		routeRepository.deleteAllInBatch();
		orderRepository.deleteAllInBatch();

		RestaurantDTO restaurant = createRestaurant();
		CustomerDTO customer = createCustomer();
		List<OrderDTO> orders = createOrders(restaurant, customer);

		Pageable pageable = PageRequest.of(0, 2);

		StringBuilder url = new StringBuilder("/orders");
		url.append(String.format("?restaurantId=%d", restaurant.getId()));
		url.append(String.format("&page=%d", pageable.getPageNumber()));
		url.append(String.format("&size=%d", pageable.getPageSize()));

		ParameterizedTypeReference<RestResponsePage<OrderDTO>> responseType = new ParameterizedTypeReference<RestResponsePage<OrderDTO>>() {};

		ResponseEntity<RestResponsePage<OrderDTO>> responseGet = restTemplate.exchange(url.toString(), HttpMethod.GET, null, responseType);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(orders.size(), responseGet.getBody().getTotalElements());
		assertEquals(pageable.getPageSize(), responseGet.getBody().getContent().size());
	}

	@Test
	public void testSearchUsingAllParameters() {
		routeRepository.deleteAllInBatch();
		orderRepository.deleteAllInBatch();

		RestaurantDTO restaurant = createRestaurant();
		CustomerDTO customer = createCustomer();
		List<OrderDTO> orders = createOrders(restaurant, customer);

		Pageable pageable = PageRequest.of(0, 2);

		StringBuilder url = new StringBuilder("/orders");
		url.append(String.format("?restaurantId=%d", restaurant.getId()));
		url.append(String.format("&deliveryStart=%s", orders.get(0).getDelivery()));
		url.append(String.format("&deliveryEnd=%s", orders.get(1).getDelivery()));
		url.append(String.format("&page=%d", pageable.getPageNumber()));
		url.append(String.format("&size=%d", pageable.getPageSize()));

		ParameterizedTypeReference<RestResponsePage<OrderDTO>> responseType = new ParameterizedTypeReference<RestResponsePage<OrderDTO>>() {};

		ResponseEntity<RestResponsePage<OrderDTO>> responseGet = restTemplate.exchange(url.toString(), HttpMethod.GET, null, responseType);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertThat(responseGet.getBody().getContent(), hasSize(2));
		assertThat(responseGet.getBody().getContent(), hasItems(orders.get(0), orders.get(1)));
	}

	@Test
	public void testSearchWhenNoResult() {
		Pageable pageable = PageRequest.of(0, 2);

		StringBuilder url = new StringBuilder("/orders");
		url.append(String.format("?restaurantId=%d", System.currentTimeMillis()));
		url.append(String.format("&page=%d", pageable.getPageNumber()));
		url.append(String.format("&size=%d", pageable.getPageSize()));

		ParameterizedTypeReference<RestResponsePage<OrderDTO>> responseType = new ParameterizedTypeReference<RestResponsePage<OrderDTO>>() {};

		ResponseEntity<RestResponsePage<OrderDTO>> responseGet = restTemplate.exchange(url.toString(), HttpMethod.GET, null, responseType);

		assertNotNull(responseGet);
		assertNotNull(responseGet.getBody());
		assertThat(responseGet.getStatusCode(), equalTo(HttpStatus.OK));
		assertEquals(0, responseGet.getBody().getTotalElements());
		assertEquals(0, responseGet.getBody().getContent().size());
	}

	private List<OrderDTO> createOrders(final RestaurantDTO restaurant, final CustomerDTO customer) {
		LocalDateTime now = LocalDateTime.now();

		List<OrderDTO> orders = new ArrayList<>();
		orders.add(new OrderDTO(restaurant.getId(), customer.getId(), now, now.plusMinutes(10)));
		orders.add(new OrderDTO(restaurant.getId(), customer.getId(), now, now.plusHours(1)));
		orders.add(new OrderDTO(restaurant.getId(), customer.getId(), now, now.plusDays(1)));

		orders.forEach((order) -> {
			ResponseEntity<Long> responsePost = restTemplate.postForEntity("/orders", order, Long.class);

			assertNotNull(responsePost);
			assertNotNull(responsePost.getBody());
			assertThat(responsePost.getStatusCode(), equalTo(HttpStatus.CREATED));

			order.setId(responsePost.getBody());
		});

		return orders;
	}

	private RestaurantDTO createRestaurant() {
		RestaurantDTO restaurant = new RestaurantDTO(2.2, 2.2);

		ResponseEntity<Long> response = restTemplate.postForEntity("/restaurants", restaurant, Long.class);

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

		restaurant.setId(response.getBody());

		return restaurant;
	}

	private CustomerDTO createCustomer() {
		CustomerDTO customer = new CustomerDTO(1.1, 1.1);

		ResponseEntity<Long> response = restTemplate.postForEntity("/customers", customer, Long.class);

		assertNotNull(response);
		assertNotNull(response.getBody());
		assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));

		customer.setId(response.getBody());

		return customer;
	}

}
