#=================================== MULTISTAGE -> build ====================================
FROM maven:3.5.2-jdk-8 AS build

COPY src /usr/src/app/src
COPY pom.xml /usr/src/app

RUN \
    mvn -f /usr/src/app/pom.xml clean package -DskipTests

#================================== MULTISTAGE -> release ===================================
FROM debian:stretch

LABEL maintainer="Raphael F. Jesus <raphaelfjesus@gmail.com>"

ARG PORT

ENV \
    PORT=${PORT:-8090} \
    JAVA_OPTS='-Xms256m -Xmx256m' \
    DEBUG_OPTS=

VOLUME /tmp

EXPOSE ${PORT}

RUN \
    apt -y update \
    && apt-get -y install openjdk-8-jre-headless curl --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build /usr/src/app/target/ifood-backend-test-1.0.0.jar /app.jar

ENTRYPOINT exec java ${JAVA_OPTS} ${DEBUG_OPTS} -Djava.security.egd=file:/dev/./urandom -jar /app.jar

HEALTHCHECK --interval=30s --timeout=30s CMD curl -f http://127.0.0.1:8090/actuator/health || exit 1

