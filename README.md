# iFood - Backend Test

[![pipeline status](https://gitlab.com/raphaelfjesus/ifood-backend-test/badges/master/pipeline.svg)](https://gitlab.com/raphaelfjesus/ifood-backend-test/commits/master)
[![bugs](https://sonarcloud.io/api/project_badges/measure?project=ifood-backend-test&branch=master&metric=bugs)](https://sonarcloud.io/component_measures?id=ifood-backend-test&metric=Reliability)
[![vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ifood-backend-test&branch=master&metric=vulnerabilities)](https://sonarcloud.io/component_measures?id=ifood-backend-test&metric=Security)
[![code smells](https://sonarcloud.io/api/project_badges/measure?project=ifood-backend-test&branch=master&metric=code_smells)](https://sonarcloud.io/component_measures?id=ifood-backend-test&metric=Maintainability)
[![coverage](https://sonarcloud.io/api/project_badges/measure?project=ifood-backend-test&branch=master&metric=coverage)](https://sonarcloud.io/component_measures?id=ifood-backend-test&metric=Coverage)
[![duplications](https://sonarcloud.io/api/project_badges/measure?project=ifood-backend-test&branch=master&metric=duplicated_lines_density)](https://sonarcloud.io/component_measures?id=ifood-backend-test&metric=Duplications)

[![quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=ifood-backend-test&branch=master)](https://sonarcloud.io/dashboard?id=ifood-backend-test)

Este documento tem como objetivo demonstrar os passos necessários para validar a implementação do [case proposto](https://github.com/ifood/ifood-backend-logistic-test) à vaga de Desenvolvedor Backend no [iFood](https://www.ifood.delivery/br/labs).

A aplicação está disponível em [https://ifood-backend-test.herokuapp.com/swagger-ui.html](https://ifood-backend-test.herokuapp.com/swagger-ui.html) para teste.

>**Nota:** É necessário aguardar alguns segundos para a aplicação responder, pois o ambiente disponibilizado no [plano gratuito](https://www.heroku.com/pricing) pelo [Heroku](https://www.heroku.com/home) entra em modo de hibernação após 30 minutos de inatividade.

![Preview do cálculo das rotas no plano cartesiano](docs/images/Preview_do_calculo_das_rotas_no_plano_cartesiano.png)

Tabela de conteúdo
==================

- [Pré-requisitos](#pré-requisitos)
- [Demonstração](#demonstração)
- [Desenvolvimento](#desenvolvimento)
- [Comandos Úteis](#comandos-úteis)
- [Referências](#referências)

## Pré-requisitos

Antes de iniciar a execução da aplicação localmente, certifique-se que as seguintes ferramentas estão disponíveis em seu ambiente:

- [Git](https://git-scm.com/)
- [Docker 1.17+](https://docs.docker.com/install/)
- [Docker Swarm](https://docs.docker.com/engine/swarm/swarm-tutorial/)
- [cURL](https://curl.haxx.se/docs/manpage.html)
- [watch](https://linux.die.net/man/1/watch)

>**Nota:** O pacote cURL e watch somente são necessários para testar a configuração da alta disponibilização da aplicação implantada no Docker Swarm.

Para assegurar que as ferramentas citadas acima estão devidamente instaladas no ambiente, execute os comandos abaixo:

```shell
# Printa a versão do Git instalado na máquina
git --version

# Printa a versão do Docker instalado na máquina
docker --version

# Printa a versão do cURL instalado na máquina
curl --version

# Printa a versão do watch instalado na máquina
watch --version
```

![Console com a confirmação da instalação das ferramentas](docs/images/Console_com_a_confirmacao_da_instalacao_das_ferramentas.png)

## Demonstração

Para simular a alta disponibilidade da aplicação, será utilizado o [Docker Swarm](https://docs.docker.com/engine/swarm/) como ferramenta de orquestração de containers Docker.

1) Inicie o docker swarm em seu ambiente, executando os comandos abaixo:

```shell
# Inicializa o orquestrador de containers na máquina
docker swarm init
```

2) Em seguida, a partir do diretório raiz do projeto, gere a imagem docker da aplicação e implante-a como serviço, conforme configuração contida no arquivo `docker-stack.yml`.

```shell
# Constrói a imagem docker da aplicação 
docker build --tag ifood-backend-test:1.0.0 .

# Execute o comando abaixo para implantar a aplicação como serviço sob o namespace IFOOD
docker stack deploy --compose-file docker-stack.yml IFOOD

# Confirme se o serviço foi criado corretamente
docker service ls
```

![Console com a confirmação da criação do serviço da aplicação](docs/images/Console_com_a_confirmacao_da_criacao_do_servico_da_aplicacao.png)

>**Nota:** Para visualizar os logs gerados pela aplicação implantada como serviço, utilize o comando `docker service logs IFOOD_backend-test` a partir do terminal.

3) Na sequência, usando seu navegador favorito, acesse a URL `http://127.0.0.1:8090/swagger-ui.html` e teste a aplicação utilizando os recursos do [Swagger](https://swagger.io/docs/).

![Página inicial do Swagger da aplicação](docs/images/Pagina_inicial_do_Swagger_da_aplicacao.png)

4) Agora, simule a queda de um dos containers associados ao serviço da aplicação e veja como a aplicação se comporta para o usuário final. Para executar esse passo, todos os comandos descritos abaixo devem ser executados em terminais diferentes.

```shell
# Diminua a réplica do serviço de 2 para 1
docker service scale IFOOD_backend-test=1

# Lista todos os containers em execução a cada segundo
watch -n 1 docker ps

# Testa o endpoint da aplicação a cada segundo
watch -n 1 curl -I http://127.0.0.1:8090/swagger-ui.html
```

![Console com a saída do monitoramento da aplicação quando simulado uma queda](docs/images/Console_com_a_saida_do_monitoramento_da_aplicacao_quando_simulado_uma_queda.png)

## Desenvolvimento

Para dar continuidade no desenvolvimento do projeto, basta importá-lo na sua IDE utilizando os recursos do [Maven](https://maven.apache.org/download.cgi) e garantir que a mesma esteja configurada para uso do [Lombok](https://projectlombok.org/).

## Comandos Úteis

- `mvn test`: Executa os testes implementados para a aplicação
- `mvn package`: Empacota a aplicação no arquivo **.jar**
- `docker swarm rm <namespace>`: Remove todos os recursos criados para o namespace informado
- `docker service ps <service_name>`: Lista todos as tasks associadas ao serviço informado

## Referências

- [Git - Documentação](https://git-scm.com/doc)
- [Docker - Documentação](https://docs.docker.com/)
- [Docker Swarm - Documentação](https://docs.docker.com/engine/swarm/)
- [Swagger - Documentação](https://swagger.io/docs/)
- [VRP - Pickup Delivery Problem](https://github.com/pgRouting/pgrouting/wiki/VRP-Pickup-Delivery-Problem)

